## **Cara Install**

`Tools yang harus dimiliki`

* Git https://git-scm.com/download/win

* Composer https://getcomposer.org/download/


> NOTE: Lakukan semua perintah dalam _command line_ dan berada pada root folder project

Clone project ini ke folder project dengan perintah :
```
> git clone https://marchelhutagalung@bitbucket.org/marchelhutagalung/chainvoting.git
```

Update composer untuk menginstall _depedency_ dengan perintah :
```
> composer update
```

Selanjutnya inisialisasi proyek ke development

```
> php init
```
Setelah itu akan muncul pesan 
     
`Which environment do you want the application to be initialized in?`  
`[0] Development` <br>
`[1] Production`

Pilih `0` untuk development, Lalu `yes` 

## Konfigurasi Database

Buka text editor anda, dan edit file `main-local.php` pada path `/common/config`

```php
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=chainvoting', //pastikan anda sudah membuat database dengan nama chainvoting
            'username' => 'root', //sesuaikan dengan username db anda
            'password' => '', //sesuaikan dengan passwword db anda
            'charset' => 'utf8',
        ],
```
Simpan configurasi tersebut lalu import `sql dump` berikut ke database anda 
https://bitbucket.org/marchelhutagalung/chainvoting/downloads/chainvotingdb.sql


## Konfigurasi Host Email
Buka text editor anda, dan edit file `main-local.php` pada path `/common/config` dengan menambahkan code berikut pada _Components_
```php
    'mailer' => [
                'class' => 'yii\swiftmailer\Mailer',
                'transport'=>[
                    'class'=>'Swift_SmtpTransport',
                    'host'=>'smtp.gmail.com',
                    'username'=>'', // email gmail anda
                    'password'=>'', // password gmail anda
                    'port'=>'465',
                    'encryption'=>'ssl',
                ],
```

## USER
`Username` : administrator

`Password` : adminchainvoting2019

## BITCOIN TESTNET

 * `Bitcoin Address` : **moU5rEGoGaTXSPWjBKWMC68aLCbz7TQr6a**
 
 * Untuk pengisian Bitcoin dapat menggunakan link berikut :
 
 https://tbtc.bitaps.com/

## Cara Akses
* Admin : `http://localhost/chainvoting/backend/web`

* Voter : `http://localhost/chainvoting/frontend/web`
