<?php
/**
 * Created by PhpStorm.
 * User: MarchelHutagalung
 * Date: 3/22/2019
 * Time: 2:10 PM
 */

namespace frontend\controllers;


use frontend\models\search\CandidateSearch;
use frontend\models\search\VoteSearch;
use frontend\models\Token;
use frontend\models\User;
use frontend\models\Vote;
use Yii;
use yii\web\Controller;

class VoterController extends Controller
{
    public function actionIndex(){
        $searchModel = new VoteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index',[
            'dataProvider'=>$dataProvider
        ]);
    }

    public function actionGetVote($id){
        $cek = Vote::find()->where(['vote_id'=>$id])->one();
        if(is_object($cek)){
            $time =date("Y-m-d H:i:s");
            $today = date('Y-m-d H:i:s',strtotime('+7 hour',strtotime($time)));
            $start = Yii::$app->formatter->asDatetime($cek->start_date,'php:Y-m-d H:i:s');
            $end = Yii::$app->formatter->asDatetime($cek->end_date,'php:Y-m-d H:i:s');
            if($today<$start){
                return $this->render('wait',['message'=>'Voting Belum dimulai']);
            }
            else if($today >= $start && $today <= $end){
                $vote_address = $cek->bitcoin_address;
                $searchModel = new CandidateSearch();
                $dataProvider = $searchModel->searchById(Yii::$app->request->queryParams,$id);
                $model = new Token();
                if($model->load(Yii::$app->request->post()) && $model->validate()) {
                    $status = Token::find()->where(['token' => $model->token])->one();

                    if(is_object($status)){
                        $tokenId = $status->token;
                        if ($status->is_used != 0 || $status->is_paid == 0) {
                            Yii::$app->session->setFlash('error', 'Maaf, Token anda telah digunakan atau belum diizinkan untuk voting');
                            return $this->render('insert',[
                                'model'=>$model,
                            ]);
                        } else {
                            return $this->render('ballot', [
                                'dataProvider' => $dataProvider,
                                'tokenId' => $tokenId,
                                'vote_address'=>$vote_address,
                            ]);
                        }
                    }
                    else{
                        Yii::$app->session->setFlash('error', 'Maaf, Token anda tidak valid.');
                        return $this->render('insert',[
                            'model'=>$model,
                        ]);
                    }
                }
                else{
                    return $this->render('insert',[
                        'model'=>$model,
                    ]);
                }
            }
            else{
                return $this->render('wait',['message'=>'Voting Telah Selesai']);
            }

        }
        else{
            return $this->redirect('error');
        }
    }

    public function actionResult($id){
        $model = Vote::find()->where(['vote_id'=>$id])->one();
        if(is_object($model)){
        return $this->render('result',
            [
                'model' => $model,
            ]);
        }
        else{
            return $this->redirect(['site/error']);
        }
    }

    public function actionError(){
        return $this->render('error');
    }
}