<?php
/**
 * Created by PhpStorm.
 * User: MarchelHutagalung
 * Date: 4/2/2019
 * Time: 7:58 PM
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class Chartist extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/';

    public $css =[
        'js/chartist/css/chartist.css',
        'js/chartist/css/chartist-custom.css',
    ];
    public $js =[
        'js/chartist/js/chartist.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $publishOptions = [
        'forceCopy'=>true,
    ];
}