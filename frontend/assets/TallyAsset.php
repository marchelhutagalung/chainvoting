<?php
/**
 * Created by PhpStorm.
 * User: MarchelHutagalung
 * Date: 4/2/2019
 * Time: 7:50 PM
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class TallyAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/site.css',
    ];

    public $js = [
        'js/Tally.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'frontend\assets\Chartist',
    ];
    public $publishOptions = [
        'forceCopy'=>true,
    ];

}