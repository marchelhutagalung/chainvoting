<?php
/**
 * Created by PhpStorm.
 * User: MarchelHutagalung
 * Date: 5/11/2019
 * Time: 10:15 PM
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class VoteAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
        'js/voting.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'frontend\assets\AppAsset',
    ];
    public $publishOptions = [
        'forceCopy'=>true,
    ];
}