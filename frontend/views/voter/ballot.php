<?php

/**
 * Created by PhpStorm.
 * User: MarchelHutagalung
 * Date: 3/21/2019
 * Time: 9:00 AM
 */
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use frontend\assets\Chartist;

Chartist::register($this);
/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */

use frontend\assets\VoteAsset;
$this->title = 'Voting';
$this->params['breadcrumbs'][] = $this->title;

VoteAsset::register($this);
?>
<div class="vote-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class'=>'table table-striped table-hover'],
        'columns' => [
            [
                'attribute' => 'candidate_id',
                'format' => 'raw',
                'label'=>'',
                'value'=> function($model){
                    return Html::img('@web/images/user.png',['style'=>'height:30px; max-width:100%']);
                },
                'contentOptions' => ['style' => 'width:80px; text-align: center;'],
                'headerOptions' => ['style' => 'text-align: center;'],
            ],
            [
                'attribute' => 'candidate_name',
                'format'=>'raw',
                'label'=>'Nama',
                'contentOptions' => ['style' => 'width:80px; text-align: center;'],
                'headerOptions' => ['style' => 'text-align: center;'],
            ],

            [
                'attribute' => 'candidate_desc',
                'format' => 'raw',
                'label' => 'Deskripsi',
            ],
            [
                'class'=>'yii\grid\ActionColumn',
                'header' => 'Aksi',
                'headerOptions' => ['style'=>'color:#3c8dbc'],
                'template' => '{voting}',
                'buttons' => [
                        'voting'=>function($url,$model){
                            $url = Url::to(['voter/voting','id'=>$model->candidate_id]);
                            //return Html::a('Vote',$url,['class'=>'btn btn-primary']);|
                            return Html::button('VOTE <span class="glyphicon glyphicon-pushpin"></span>',['value'=>$model->candidate_id,'class'=>'btn btn-sm btn-primary voteBtn']);
                        }
                ]

            ]
        ],
    ])
    ?>
     <input type="hidden" id="tokenId" value="<?=$tokenId;?>">
    <input type="hidden" class="voteId" value="<?=$_GET['id'];?>">
    <input type="hidden" id="vote_address" value="<?=$vote_address?>">
</div>



