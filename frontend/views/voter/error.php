<?php

/**
 * Created by PhpStorm.
 * User: MarchelHutagalung
 * Date: 3/22/2019
 * Time: 3:10 PM
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */
$name = 'Not Found (#404)';
$this->title = $name;
?>
<section class="content">
    <div class="error-page">
        <div class="error-content">
            <h1><?= $name ?></h1>

            <h5 class="alert alert-danger">
                Voting tidak ditemukan silahkan cek email anda untuk mendapatkan <b>url </b> yang valid
            </h5>
            <a href='<?= Yii::$app->homeUrl ?>' class="btn btn-primary">Kembali ke Halaman utama</a>
        </div>
    </div>

</section>
