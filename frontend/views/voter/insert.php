<?php

/**
 * Created by PhpStorm.
 * User: MarchelHutagalung
 * Date: 3/25/2019
 * Time: 9:40 AM
 */

/* @var $this \yii\web\View */
/* @var $model \backend\models\Token */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
$this->title = 'Token';
?>

<div class="token-form">
    <?php
    $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}\n{hint}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
        //'enableAjaxValidation'=>true,
    ]) ?>
    <center>
        <h4>MASUKKAN TOKEN ANDA</h4>
    </center>
    <?= $form->field($model,'token')->textInput(['autofocus'=>true])->label('')?>
    <div class="form-group">
        <div class="col-sm-8 col-md-offset-2">
            <?= Html::submitButton('ENTER', ['class' => 'btn btn-primary form-control', 'name' => 'contact-button']) ?>
        </div>
    </div>
    <style>
        .form-group{
            margin-bottom: 0px;
        }
        .token-form{
            padding-top: 200px;
        }
    </style>

    <?php ActiveForm::end();?>
</div>
