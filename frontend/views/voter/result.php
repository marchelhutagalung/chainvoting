<?php

/**
 * Created by PhpStorm.
 * User: MarchelHutagalung
 * Date: 4/3/2019
 * Time: 3:50 PM
 */

use aneeshikmat\yii2\Yii2TimerCountDown\Yii2TimerCountDown;
use frontend\assets\TallyAsset;
use yii\bootstrap\Html;

/* @var $this \yii\web\View */
$this->title = 'Perhitungan';
TallyAsset::register($this);
?>

<div class="row">
    <div class="col-md-12">
        <div class="jumbotron">
            <div class="panel panel-headline">
                <input type="hidden" class="vote_id" value="<?=$_GET['id']?>">
                <input type="hidden" class="admaddress" value="<?=$model->bitcoin_address;?>">
                <input type="hidden" class="vtnumbers" value="<?=$model->vote_numbers;?>">
                <div class="panel-heading">
                    <h2 class="panel-title"> <b>PENGHITUNGAN </b></h2>
                    <p class="panel-subtitle"><strong></strong><span class="tally-status" id="tallys"></span></p>
                    <span class="percentage"></span>
                </div>
                <?php
                $date = date('Y-m-d H:i:s',strtotime('-7 hour',strtotime($model->end_date)));
                $callBackScript = <<<JS
            document.getElementById('time-down-counter').className += "label label-danger";
JS;
                ?>

                <div id="time-down-counter"></div>
                <?= Yii2TimerCountDown::widget([
                    'countDownIdSelector' => 'time-down-counter',
                    'countDownDate' => strtotime($date) * 1000,
                    'countDownResSperator' => ':',
                    'addSpanForResult' => false,
                    'addSpanForEachNum' => false,
                    'countDownOver' => 'Selesai',
                    'countDownReturnData' => 'from-days',
                    'templateStyle' => 2,
                    'getTemplateResult' => 0,
                    'callBack' => $callBackScript
                ]) ?>
        </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="ct-chart ct-perfect-fourth"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="ct-bar-chart ct-perfect-fourth"></div>
                    </div>
                </div>
                <hr>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" class="vote-candidate" value="" />
                        <div class="list-candidate">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
