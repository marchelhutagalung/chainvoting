<?php

/**
 * Created by PhpStorm.
 * User: MarchelHutagalung
 * Date: 3/22/2019
 * Time: 3:20 PM
 */

/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */

use aneeshikmat\yii2\Yii2TimerCountDown\Yii2TimerCountDown;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

$this->title = 'Daftar Voting';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vote-index">
    <p>
        <?php // Html::a('Create Vote', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover'],
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'vote_id',
            [
                'attribute' => 'vote_title',
                'value' => 'vote_title',
                'label' => 'Judul',
            ],
            [
                'attribute' => 'vote_desc',
                'format' => 'raw',
                'label' => 'Deskripsi',
            ],
            [
                'format' => 'raw',
                'label' => 'Status',
                'headerOptions' => ['style' => 'color:#3c8dbc'],
                'value'=>function($model){
                    $time =date("Y-m-d H:i:s");
                    $today = date('Y-m-d H:i:s',strtotime('+7 hour',strtotime($time)));
                    $start = Yii::$app->formatter->asDatetime($model->start_date,'php:Y-m-d H:i:s');
                    $end = Yii::$app->formatter->asDatetime($model->end_date,'php:Y-m-d H:i:s');

                    if($today<$start){
                        return '<span class="label label-warning">Menunggu</span>';
                    }
                    else if($today >= $start && $today <= $end){
                        return '<span class="label label-primary">Sedang Mulai</span>';
                    }
                    else{
                        return '<span class="label label-success">Selesai</span>';
                    }
                }
            ],
//            [
//                'attribute' => 'end_date',
//                'label'=>'Timer',
//                'format'=>'raw',
//                'value'=>function($model){
//$callBackScript = <<<JS
//        document.getElementById('time-down-counter').className += "label label-danger";
//JS;
//                    $date = date('Y-m-d H:i:s',strtotime('-7 hour',strtotime($model->end_date)));
//                    return '
//                            <div id="time-down-counter" class=""></div>'.Yii2TimerCountDown::widget([
//                            'countDownIdSelector' => 'time-down-counter',
//                            'countDownDate' => strtotime($date) * 1000,
////                            'countDownDate' => strtotime("+1 minute") * 1000,
//                            'countDownResSperator' => ':',
//                            'countDownOver' => '00:00:00',
//                            'countDownReturnData' => 'from-days',
//                            'templateStyle' => 0,
//                            'callBack' => $callBackScript
//                        ]);
//                }
//            ],
            [
                'class'=>'yii\grid\ActionColumn',
                'header'=>'Aksi',
                'headerOptions' => ['style'=>'color:#3c8dbc'],
                'template' => '{hasil}{vote}',
                'buttons' =>
                [
                    'hasil'=>function($url,$model){
                        $url = Url::to(['voter/result','id'=>$model->vote_id]);
                        $time =date("Y-m-d H:i:s");
                        $today = date('Y-m-d H:i:s',strtotime('+7 hour',strtotime($time)));
                        $start = Yii::$app->formatter->asDatetime($model->start_date,'php:Y-m-d H:i:s');
                        $end = Yii::$app->formatter->asDatetime($model->end_date,'php:Y-m-d H:i:s');

                        if($today<$start){
                            return '';
                        }
                        else if($today >= $start && $today <= $end){
                            return Html::a('<span class="glyphicon glyphicon-search"></span> Hasil',$url,['class'=>'label label-primary','style'=>'margin-right : 10px;']);
                        }
                        else{
                            return Html::a('<span class="glyphicon glyphicon-search"></span> Hasil',$url,['class'=>'label label-primary']);
                        }

                    },
                    'vote'=>function($url,$model){
                        $url = Url::to(['voter/get-vote','id'=>$model->vote_id]);
                        $time =date("Y-m-d H:i:s");
                        $today = date('Y-m-d H:i:s',strtotime('+7 hour',strtotime($time)));
                        $start = Yii::$app->formatter->asDatetime($model->start_date,'php:Y-m-d H:i:s');
                        $end = Yii::$app->formatter->asDatetime($model->end_date,'php:Y-m-d H:i:s');

                        if($today<$start){
                            return '';
                        }
                        else if($today >= $start && $today <= $end){
                            return Html::a('<span class="glyphicon glyphicon-pushpin"></span> Vote',$url,['class'=>'label label-success']);
                        }
                        else{
                            return '';
                        }
                    }
                ]
            ]
        ],
    ]);
    ?>
</div>

