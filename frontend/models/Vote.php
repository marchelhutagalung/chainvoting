<?php

namespace frontend\models;

use common\behaviors\BlameableBehavior;
use common\behaviors\DeleteBehavior;
use common\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "vote".
 *
 * @property string $vote_id
 * @property string $vote_title
 * @property string $vote_desc
 * @property int $vote_numbers
 * @property string $start_date
 * @property string $end_date
 * @property string $bitcoin_address
 * @property int $deleted
 * @property string $deleted_at
 * @property string $deleted_by
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property Candidate[] $candidates
 * @property Token[] $tokens
 */
class Vote extends \yii\db\ActiveRecord
{
    /**
     * behaviour to add created_at and updatet_at field with current datetime (timestamp)
     * and created_by and updated_by field with current user id (blameable)
     */
    public function behaviors(){
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
            ],
            'delete' => [
                'class' => DeleteBehavior::className(),
            ]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vote';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vote_id', 'vote_title', 'vote_numbers', 'start_date', 'end_date', 'bitcoin_address'], 'required'],
            [['vote_desc'], 'string'],
            [['vote_numbers', 'deleted'], 'integer'],
            [['start_date', 'end_date', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['vote_id'], 'string', 'max' => 3],
            [['vote_title', 'bitcoin_address'], 'string', 'max' => 255],
            [['deleted_by', 'created_by', 'updated_by'], 'string', 'max' => 32],
            [['vote_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vote_id' => 'Vote ID',
            'vote_title' => 'Nama Voting',
            'vote_desc' => 'Deskripsi',
            'vote_numbers' => 'Jumlah',
            'start_date' => 'Tanggal Mulai',
            'end_date' => 'Tanggal Berakhir',
            'bitcoin_address' => 'Bitcoin Address',
            'deleted' => 'Deleted',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidates()
    {
        return $this->hasMany(Candidate::className(), ['vote_id' => 'vote_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokens()
    {
        return $this->hasMany(Token::className(), ['vote_id' => 'vote_id']);
    }


}
