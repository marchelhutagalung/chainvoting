<?php

namespace frontend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Vote;

/**
 * VoteSearch represents the model behind the search form of `backend\models\Vote`.
 */
class VoteSearch extends Vote
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vote_id', 'vote_title', 'vote_desc', 'start_date', 'end_date', 'bitcoin_address', 'deleted_at', 'deleted_by', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'safe'],
            [['vote_numbers', 'deleted'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vote::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'vote_numbers' => $this->vote_numbers,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'vote_id', $this->vote_id])
            ->andFilterWhere(['like', 'vote_title', $this->vote_title])
            ->andFilterWhere(['like', 'vote_desc', $this->vote_desc])
            ->andFilterWhere(['like', 'bitcoin_address', $this->bitcoin_address])
            ->andFilterWhere(['like', 'deleted_by', $this->deleted_by])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
}
