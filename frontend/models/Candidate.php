<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "candidate".
 *
 * @property int $candidate_id
 * @property string $candidate_name
 * @property string $candidate_desc
 * @property int $vote_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Vote $vote
 */
class Candidate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'candidate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['candidate_name', 'vote_id'], 'required'],
            [['vote_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['candidate_name', 'candidate_desc'], 'string', 'max' => 255],
            [['vote_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vote::className(), 'targetAttribute' => ['vote_id' => 'vote_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'candidate_id' => 'Candidate ID',
            'candidate_name' => 'Candidate Name',
            'candidate_desc' => 'Candidate Desc',
            'vote_id' => 'Vote ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVote()
    {
        return $this->hasOne(Vote::className(), ['vote_id' => 'vote_id']);
    }
}
