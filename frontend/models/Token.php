<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "token".
 *
 * @property int $id
 * @property string $token
 * @property string $public_key
 * @property int $is_used
 * @property int $vote_id
 *
 * @property Vote $vote
 */
class Token extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'token';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_used', 'vote_id'], 'integer'],
            [['token'], 'string', 'max' => 11],
            [['bitcoin_pubkey', 'bitcoin_pvkey', 'bitcoin_address'], 'string', 'max' => 255],
            [['vote_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vote::className(), 'targetAttribute' => ['vote_id' => 'vote_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'token' => 'Token',
            'bitcoin_pubkey' => 'Bitcoin Pubkey',
            'bitcoin_pvkey' => 'Bitcoin Pvkey',
            'bitcoin_address' => 'Bitcoin Address',
            'is_used' => 'Is Used',
            'vote_id' => 'Vote ID',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVote()
    {
        return $this->hasOne(Vote::className(), ['vote_id' => 'vote_id']);
    }
}
