/**
 * Tally
 */
$(document).ready(function () {
    $('#LoadingImage').show();
    tallyingVoting();
    getCandidate();
    createChart();
    createBarChart();
    data.labels = [];
    data.series = [];
    data.id =[];
    barData.series = [];
    //output : Localhost;
    // console.log(window.location.hostname);

    //output : /ChainVoting/frontend/web/voter/result
    // console.log(window.location.pathname);

});

/**
 *
 */
function createChart() {
    char = new Chartist.Bar('.ct-chart', data , {
        distributeSeries: true
    });
}

/**
 *
 */
function createBarChart() {
    var sum = function(a, b) { return a + b };

    barchart = new Chartist.Pie('.ct-bar-chart', data, {
        labelInterpolationFnc: function(value) {
            return Math.round(value / data.series.reduce(sum) * 100) + '%';
        }
    });
}

/**
 *
 */
function tallyingVoting(){
    admaddress = $('.admaddress').val();
    totalVoters = $('.vtnumbers').val();
    count = 0;
    $.getJSON("https://testnet-api.smartbit.com.au/v1/blockchain/address/"+admaddress+"?limit=1000",function (result) {
        if (result.success === true && result.address.total.transaction_count >0) {
            var item =  result.address.transactions;
                for(var i=0; i<item.length;i++){
                    (function(i){
                        setTimeout(function(){
                            if(i === item.length - 1 ){
                                $('#LoadingImage').hide();
                            }else{
                            }
                            counting(item[i].outputs[0].script_pub_key.hex);
                        },i*200);
                    })(i);
            }
        }
        else{
            $('#LoadingImage').hide();
            $(".tally-status").html('Data Belum Masuk').addClass('label label-danger');
            $(".percentage").html('0%').addClass('badge');
        }
    });
}

/**
 *
 * @param hex
 */
function counting(hex){
    var item = hex2bin(hex);
    console.log('OP_RETURN : '+hex);
    console.log('Decode(OP_RETURN) : '+item);

    var voteId = item.substring(8,11);
    var candidateId = Number(item.substring(12,item.length));
    console.log("Voting_id : "+voteId);
    console.log("Candidate_id : "+candidateId);
    console.log('\n');
    if( voteId === data.vote_id){
        for(var i = 0;i<data.id.length;i++){
            if(Number(data.id[i]) === candidateId){
                data.series[i] +=1;
                barData.series[i]+=1;
                count++;
                $(".tally-status").html('Berikut Data yang telah masuk : '+count+' dari '+ totalVoters).addClass('label label-success');
                $(".percentage").html(Math.round(count/totalVoters*100)+'% Suara').addClass('badge');
                $(".candidate-vote-"+candidateId).html(data.series[i]);
            }
        }
        if(Number(totalVoters) === Number(count)){
            $(".badge").css("background-color", "#5cb85c");
        }
        char.update(data);
        barchart.update(barData);
    }

}

/**
 *
 */
function getCandidate(){
    data = {vote_id : $(".vote_id").val()};
    barData = {vote_id : $(".vote_id").val()};
    $.getJSON("/ChainVoting/backend/web/api/candidate/get?id="+data.vote_id,function (result) {
        if(result.status === true){
            var str ='';
            $(result.content).each(function(i,item){
                str +='<div class="col-md-3"><div class="panel panel-success">' +
                    '<div class="panel-heading"><h3 class="panel-title">'
                    + item.candidate_name +
                    '</p></div><div class="panel-body"><p class="candidate-vote candidate-vote-' +  item.candidate_id +'">'
                    +
                    '</p></div></div></div>';
                data.labels.push(item.candidate_name);
                data.id.push(item.candidate_id);
                data.series.push(0);
                barData.series.push(0);
            });
            $(".list-candidate").html(str);
            $(".candidate-vote").html("0");
        }
    })
}