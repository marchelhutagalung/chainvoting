/**
 * Fungsi untuk melakukan Voting
 */

$(document).ready(function () {
    var token = $('#tokenId').attr('value');
    var voteId = $('.voteId').attr('value');
    var vote_address = $('#vote_address').attr('value');

    console.log(vote_address);
    console.log(voteId);
    console.log(token);
    $('.voteBtn').click(function(){
        $('#LoadingImage').show();
        var candidateId = $(this).attr('value');
        $.getJSON("/ChainVoting/backend/web/api/token/private?tkn="+token,function (result) {
            wif = result.content.bitcoin_pvkey;
            network = Bitcoin.networks.testnet;
            keyPair = Bitcoin.ECPair.fromWIF(wif,network);
            var bitcoinAddress = keyPair.getAddress();
            console.log(bitcoinAddress);
            console.log(voteId);
            $.getJSON("https://chain.so/api/v2/get_tx_unspent/BTCTEST/"+bitcoinAddress,function(result){
                var txb = new Bitcoin.TransactionBuilder(network);
                var last = result.data.txs.length-1;
                unspent_txid = result.data.txs[last].txid;
                unspent_vout = result.data.txs[last].output_no;
                txb.addInput(unspent_txid, unspent_vout);
                value = Number(result.data.txs[last].value * 100000000);
                // address of vote = "moU5rEGoGaTXSPWjBKWMC68aLCbz7TQr6a";
                address = vote_address;
                pay = 0.00001 * 100000000;
                change = parseInt(value - pay);
                console.log(change);
                let commit = new Buffered("voting"+voteId+padding(candidateId,3));
                let dataScript = Bitcoin.script.nullData.output.encode(commit);
                txb.addOutput(dataScript, 0);
                txb.addOutput(address,change);
                txb.sign(0, keyPair);
                let txRaw = txb.build();
                let txHex = txRaw.toHex();
                postdata = {tx_hex : txHex};
                $.post("https://chain.so/api/v2/send_tx/BTCTEST/",postdata,function(result){
                    if(result.status == "success" ){
                        setTimeout(function(){
                            $.get("/ChainVoting/backend/web/api/token/used?tkn="+token,function (result) {
                                if(result.status === true){
                                    window.open("/ChainVoting/frontend/web/voter/result?id="+voteId,"_self");
                                    $('#LoadingImage').hide();
                                }
                            })
                        }, 3000);
                    }
                });
            });
        });
    });
});


