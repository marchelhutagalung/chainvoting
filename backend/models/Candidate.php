<?php

namespace backend\models;

use common\behaviors\BlameableBehavior;
use common\behaviors\DeleteBehavior;
use common\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "candidate".
 *
 * @property int $candidate_id
 * @property string $candidate_name
 * @property string $candidate_desc
 * @property string $vote_id
 * @property int $deleted
 * @property string $deleted_at
 * @property string $deleted_by
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property Vote $vote
 */
class Candidate extends \yii\db\ActiveRecord
{
    /**
     * behaviour to add created_at and updatet_at field with current datetime (timestamp)
     * and created_by and updated_by field with current user id (blameable)
     */

    public function behaviors()
    {
        return [
            'timestamp'=>[
                'class'=>TimestampBehavior::className(),
            ],
            'blameable'=>[
                'class'=>BlameableBehavior::className(),
            ],
            'delete'=>[
                'class'=>DeleteBehavior::className(),
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'candidate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['candidate_name'], 'required'],
            [['deleted'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['candidate_name', 'candidate_desc'], 'string', 'max' => 255],
            [['vote_id'], 'string', 'max' => 3],
            [['deleted_by', 'created_by', 'updated_by'], 'string', 'max' => 32],
            [['vote_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vote::className(), 'targetAttribute' => ['vote_id' => 'vote_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'candidate_id' => 'Candidate ID',
            'candidate_name' => 'Nama Kandidat',
            'candidate_desc' => 'Deskripsi',
            'vote_id' => 'Voting',
            'deleted' => 'Deleted',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVote()
    {
        return $this->hasOne(Vote::className(), ['vote_id' => 'vote_id']);
    }
}
