<?php

namespace backend\models;

use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use Yii;
use yii\base\Model;

/**
 * Voter is the model behind the voter
 */

class Voter extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $vote_id;

    /**
     * {@inheritdoc}
     */
     public function rules(){
         return [
             [['name','email','vote_id'],'required'],
             [['subject','body','vote_id'],'string'],
             ['email','email'],
         ];
     }


     /**
     * {@inheritdoc}
     */
     public function attributeLabels(){
         return [
             'verifyCode'=>'Verification Code',
             'name'=>'Nama Pemilih',
         ];
     }

     public function sendMail($email){
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([Yii::$app->params['adminEmail'] => 'Admin Voting'])
            ->setSubject($this->subject)
            ->setHtmlBody($this->body)
            ->send();
     }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVote()
    {
        return $this->hasOne(Vote::className(), ['vote_id' => 'vote_id']);
    }

    public function generateToken($n, $vote_id)
    {
        $model = new Token();
        $number = intval($n);
        $bitcoin = new BitcoinECDSA();
        $bitcoin->setNetworkPrefix('6f');
        if ($n > 0 && $number < 1000) {
            for ($i = 0; $i < $n; $i++) {
                $token = $model->randomToken();
                $model->token = $token;
                $model->vote_id = $vote_id;
                $bitcoin->generateRandomPrivateKey();
                $model->bitcoin_pubkey = $bitcoin->getPubKey();
                $model->bitcoin_pvkey = $bitcoin->getWif();
                $model->bitcoin_address = $bitcoin->getAddress();
                if ($model->save(false)) {
                    return $model->token;
                }
            }
        }
        return false;
    }
}
?>