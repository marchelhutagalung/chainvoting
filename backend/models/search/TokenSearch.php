<?php

namespace backend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Token;

/**
 * TokenSearch represents the model behind the search form of `backend\models\Token`.
 */
class TokenSearch extends Token
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'is_used', 'is_paid', 'deleted'], 'integer'],
            [['token', 'bitcoin_pubkey', 'bitcoin_pvkey', 'bitcoin_address', 'vote_id', 'deleted_at', 'deleted_by', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $time =date("Y-m-d H:i:s");
        $today = date('Y-m-d H:i:s',strtotime('+7 hour',strtotime($time)));
        $query = Token::find()->joinWith('vote')->where(['is_used' => 0])->andWhere(['>=','vote.end_date',$today]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_used' => $this->is_used,
            'is_paid' => $this->is_paid,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'bitcoin_pubkey', $this->bitcoin_pubkey])
            ->andFilterWhere(['like', 'bitcoin_pvkey', $this->bitcoin_pvkey])
            ->andFilterWhere(['like', 'bitcoin_address', $this->bitcoin_address])
            ->andFilterWhere(['like', 'vote.vote_id', $this->vote_id])
            ->andFilterWhere(['like', 'deleted_by', $this->deleted_by])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchex($params)
    {
        $time =date("Y-m-d H:i:s");
        $today = date('Y-m-d H:i:s',strtotime('+7 hour',strtotime($time)));
        $query = Token::find()->joinWith('vote')->where(['is_used' => 0])->andWhere(['<=','vote.end_date',$today]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_used' => $this->is_used,
            'is_paid' => $this->is_paid,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'bitcoin_pubkey', $this->bitcoin_pubkey])
            ->andFilterWhere(['like', 'bitcoin_pvkey', $this->bitcoin_pvkey])
            ->andFilterWhere(['like', 'bitcoin_address', $this->bitcoin_address])
            ->andFilterWhere(['like', 'vote.vote_id', $this->vote_id])
            ->andFilterWhere(['like', 'deleted_by', $this->deleted_by])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
}
