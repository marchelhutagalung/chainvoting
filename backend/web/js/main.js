/**
 * Bayar per satuan voter
 */
$('.validation').click(function(){
    let id = $(this).attr('value');
    // Mengambil bitcoin address dari voter
    $.getJSON("/ChainVoting/backend/web/api/token/get-address?id="+id,function (result) {
        if(result.status === true){
            address = result.data.bitcoin_address;
            pub_key = result.data.bitcoin_pubkey;
        }
    });
    $.getJSON('http://localhost/ChainVoting/backend/web/api/user/admin',function(result) {
        wif = result.data.bitcoin_private_key;
        network = Bitcoin.networks.testnet;
        keyPair = Bitcoin.ECPair.fromWIF(wif,network);
        bitcoinAddress = keyPair.getAddress();
        console.log(bitcoinAddress);
        success = result.status;
        if(success === true){
            $.getJSON("https://chain.so/api/v2/get_tx_unspent/BTCTEST/"+bitcoinAddress,function(resultBtc){
                let last = resultBtc.data.txs.length - 1;
                let unspent_txid = resultBtc.data.txs[last].txid;
                let unspent_vout = resultBtc.data.txs[last].output_no;
                txb = new Bitcoin.TransactionBuilder(network);
                txb.addInput(unspent_txid,unspent_vout);
                let commit = new Buffered(Sha1.hash(pub_key));
                let dataScript = Bitcoin.script.nullData.output.encode(commit);
                value = Number(resultBtc.data.txs[last].value * 100000000);
                pay = 0.0001 * 100000000;
                change = parseInt(value - pay);
                console.log(change);
                txb.addOutput(dataScript,0);
                txb.addOutput(address,change);
                txb.sign(0,keyPair);
                var txRaw = txb.build();
                var txHex = txRaw.toHex();
                postdata = {tx_hex: txHex};
                postValidation(id,postdata);
            })
        }
    })
});

$('#payall').click(function () {
    jQuery('#contentPege').showLoading();
    vote_id = $('#tokensearch-vote_id').find(":selected").val();
    $.getJSON('http://localhost/ChainVoting/backend/web/api/user/admin',function(result) {
        var count = 0;
        wif = result.data.bitcoin_private_key;
        network = Bitcoin.networks.testnet;
        keyPair = Bitcoin.ECPair.fromWIF(wif,network);
        bitcoinAddress = keyPair.getAddress();
        $.getJSON("/ChainVoting/backend/web/api/token/token?id="+vote_id,function (result) {
            if(result.status === false){
                alert('Bitcoin address pada voting ini sudah dibayar');
                jQuery('#contentPege').hideLoading();
            }else{
                sum = result.content.length;
                count = 0;
                $(result.content).each(function(i,item){
                    setTimeout(function () {
                        $.getJSON("https://chain.so/api/v2/get_tx_unspent/BTCTEST/"+bitcoinAddress,function(resultBtc){
                            let last = resultBtc.data.txs.length - 1;
                            let unspent_txid = resultBtc.data.txs[last].txid;
                            let unspent_vout = resultBtc.data.txs[last].output_no;
                            txb = new Bitcoin.TransactionBuilder(network);
                            txb.addInput(unspent_txid,unspent_vout);
                            let commit = new Buffered(Sha1.hash(item.bitcoin_pubkey));
                            let dataScript = Bitcoin.script.nullData.output.encode(commit);
                            value = Number(resultBtc.data.txs[last].value * 100000000);
                            pay = 0.0001 * 100000000;
                            change = parseInt(value - pay);
                            console.log(change);
                            txb.addOutput(dataScript,0);
                            txb.addOutput(item.bitcoin_address,change);
                            txb.sign(0,keyPair);
                            var txRaw = txb.build();
                            var txHex = txRaw.toHex();
                            postdata = {tx_hex: txHex};
                            count++;
                            console.log("Sum "+sum)
                            console.log("Count : "+count)
                            postAll(item.bitcoin_address,postdata,count,sum);
                        });
                    }, i*2000);
                });
            }
        });
    });
});

/**
 * membuat transaksi ke API
 * @param itemId
 * @param postdata
 */
function postValidation(itemId,postdata){
    jQuery('#contentPege').showLoading();
    $.post("https://chain.so/api/v2/send_tx/BTCTEST/",postdata,function (result) {
        if(result.status === "success"){
            $.getJSON("http://localhost/ChainVoting/backend/web/api/token/paid?id="+itemId,function () {
                if(result.status==="success"){
                    document.location.reload(true);
                }
            });
        }
    })
}

// Post all
function postAll(itemId,postdata,count,sum){
    console.log(postdata)
    $.post("https://chain.so/api/v2/send_tx/BTCTEST/",postdata,function (result) {
        if(result.status === "success"){
            $.getJSON("http://localhost/ChainVoting/backend/web/api/token/active?id="+itemId,function () {
                if(count == sum){
                    jQuery('#contentPege').hideLoading();
                    document.location.reload(true);
                }
            });
        }
    });
}

