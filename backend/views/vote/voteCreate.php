<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Vote */

$this->title = 'Tambah Vote';
$this->params['breadcrumbs'][] = ['label' => 'Votes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$uiHelper=\Yii::$app->uiHelper;
?>
<div class="vote-create">
    <?=$uiHelper->beginContentRow() ?>

    <?=$uiHelper->beginContentBlock(['id' => 'grid-system2',
        'width' => 12,
        'type' => 'success'
    ]); ?>
    <?= $this->render('_voteCreate', [
        'modelVote'=> $modelVote,
        'modelsCandidate'=>$modelsCandidate,
        'modelImport'=>$modelImport,
    ]) ?>
    <?= $uiHelper->endContentBlock()?>
    <?=$uiHelper->endContentRow() ?>
</div>
