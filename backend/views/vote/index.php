<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\VoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Voting';
$this->params['breadcrumbs'][] = $this->title;
$uiHelper=\Yii::$app->uiHelper;
?>
<div class="vote-index">
    <?=$uiHelper->beginContentRow() ?>

    <?=$uiHelper->beginContentBlock(['id' => 'grid-system2',
        'width' => 12,
        'type' => 'success'
    ]); ?>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php // Html::a('Create Vote', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'tableOptions' => ['class'=>'table table-hover'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'vote_id',
            [
                    'attribute' => 'vote_title',
                    'value' => 'vote_title',
            ],
            [
                'attribute' => 'vote_desc',
                'format' => 'raw',
            ],
            [
                'format' => 'raw',
                'label' => 'Status',
                'headerOptions' => ['style' => 'color:#3c8dbc'],
                'value'=>function($model){
                    $time =date("Y-m-d H:i:s");
                    $today = date('Y-m-d H:i:s',strtotime('+7 hour',strtotime($time)));

                    $start = Yii::$app->formatter->asDatetime($model->start_date,'php:Y-m-d H:i:s');
                    $end = Yii::$app->formatter->asDatetime($model->end_date,'php:Y-m-d H:i:s');

                    if($today<$start){
                        return '<span class="label label-warning">Menunggu</span>';
                    }
                    else if($today >= $start && $today <= $end){
                        return '<span class="label label-primary">Sedang Mulai</span>';
                    }
                    else{
                        return '<span class="label label-success">Selesai</span>';
                    }
                }
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{view}{result}',
                'buttons'=>[
                    'view' => function ($url,$model) {
                        $url = Url::to(['vote/view', 'id' => $model->vote_id]);
                        return Html::a('Rincian', $url,['class'=>'btn btn-sm btn-primary','style'=>'margin-right : 5px;']);
                    },
                    'result'=> function($url,$model){
                        $time =date("Y-m-d H:i:s");
                        $today = date('Y-m-d H:i:s',strtotime('+7 hour',strtotime($time)));
                        $start = Yii::$app->formatter->asDatetime($model->start_date,'php:Y-m-d H:i:s');
                        $end = Yii::$app->formatter->asDatetime($model->end_date,'php:Y-m-d H:i:s');
                        $url = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/voter/result?id=').$model->vote_id;
                        if($today<$start){
                            return '';
                        }
                        else if($today >= $start && $today <= $end){
                            return Html::a('Hasil Sementara', $url,['class'=>'btn btn-sm btn-success','target'=>'_blank']);
                        }
                        else{
                            return Html::a('Hasil', $url,['class'=>'btn btn-sm btn-success','target'=>'_blank']);
                        }
                    }
                ]
            ],
        ],
    ]);
    ?>
    <?= $uiHelper->endContentBlock()?>

    <?=$uiHelper->endContentRow() ?>
</div>
