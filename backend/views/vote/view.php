<?php

use aneeshikmat\yii2\Yii2TimerCountDown\Yii2TimerCountDown;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use backend\models\Candidate;

/* @var $this yii\web\View */
/* @var $model backend\models\Vote */

$this->title = $model->vote_title;
$this->params['breadcrumbs'][] = ['label' => 'Voting', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$uiHelper=\Yii::$app->uiHelper;
?>

<div class="vote-view">
    <?=$uiHelper->beginContentRow() ?>

    <?=$uiHelper->beginContentBlock(['id' => 'grid-system2',
        'width' => 12,
        'type' => 'success'
    ]); ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'vote_title',
            ],
            [
                'attribute'=>'vote_desc',
                'value'=>$model->vote_desc,
                'format'=>'raw',
            ],
            [
                'attribute'=>'start_date',
                'format'=>'raw',
                'value'=>function($model){
                    return Yii::$app->formatter->asDate($model->start_date,'php:d/m/Y - H:i').' WIB';
                }
            ],
            [
                'attribute'=>'end_date',
                'format'=>'raw',
                'value'=>function($model){
                    return Yii::$app->formatter->asDate($model->end_date,'php:d/m/Y - H:i').' WIB';
                }
            ],
            [
                'attribute'=>'vote_numbers',
                'format'=>'raw',
                'value'=>function($model){
                    return $model->vote_numbers.' Orang';
                }
            ],
            [
                'label'=>'Status',
                'format'=>'raw',
                'value'=>function($model){
$callBackScript = <<<JS
        document.getElementById('time-down-counter').className += "label label-success";
JS;
                    $date = date('Y-m-d H:i:s',strtotime('-7 hour',strtotime($model->end_date)));
                    return '
                            <div id="time-down-counter" class=""></div>'.Yii2TimerCountDown::widget([
                            'countDownIdSelector' => 'time-down-counter',
                            'countDownDate' => strtotime($date) * 1000,
//                            'countDownDate' => strtotime("+1 minute") * 1000,
                            'countDownResSperator' => ':',
                            'countDownOver' => 'Selesai',
                            'countDownReturnData' => 'from-days',
                            'templateStyle' => 2,
                            'callBack' => $callBackScript
                        ]);
                }
            ],
        ],
    ]) ?>
    <p><br>
        <?php $url = $url = Yii::$app->urlManagerFrontEnd->createAbsoluteUrl('/voter/result?id=').$model->vote_id; ?>
        <?= Html::a('Edit',['vote-update','id'=>$model->vote_id], ['class' => 'btn btn-primary']) ?>
         <?= Html::a('Hasil',$url, ['class' => 'btn btn-success','target'=>'_blank']) ?>
    </p>
    <hr/>
    <h2><?= Html::encode("Daftar Kandidat") ?></h2>

    <?php
    $dataProvider = new ActiveDataProvider([
        'query' => Candidate::find()->where(['vote_id'=>$_GET['id']]),
        'pagination' => [
            'pageSize' => 20,
        ],
    ]);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-stripped table-condensed'],
        'columns' => [
            'candidate_name',
            [
                'attribute' => 'candidate_desc',
                'format' => 'raw',
            ],
        ],
    ]);
    ?>

    <?= $uiHelper->endContentBlock()?>

    <?=$uiHelper->endContentRow() ?>
</div>
