<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Vote */

$this->title = 'Tambah Vote';
$this->params['breadcrumbs'][] = ['label' => 'Votes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vote-create">

    <?= $this->render('_voteUpdate', [
        'modelVote'=> $modelVote,
        'modelsCandidate'=>$modelsCandidate,
    ]) ?>

</div>
