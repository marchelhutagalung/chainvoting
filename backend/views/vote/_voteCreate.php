<?php

/**
 * Created by PhpStorm.
 * User: MarchelHutagalung
 * Date: 4/25/2019
 * Time: 9:18 PM
 */

/* @var $this \yii\web\View */
/* @var $model \backend\models\Vote|\yii\db\ActiveRecord */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\datetime\DateTimePicker;
$datetime = new DateTime();
$js = '
jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
        jQuery(this).html("Kandidat : " + (index + 1))
    });
});

jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
        jQuery(this).html("Kandidat: " + (index + 1))
    });
});

jQuery("#add-vote").click(function(){
    jQuery("#contentPege").showLoading();
});
';
use nirvana\showloading\ShowLoadingAsset;
ShowLoadingAsset::register($this);
$this->registerJs($js);
?>
<div class="vote-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
            <?= $form->field($modelVote, 'vote_title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($modelVote, 'vote_desc')->widget(\yii\redactor\widgets\Redactor::className()) ?>
    <div class="row">
        <div class="col-sm-6">
                <?= $form->field($modelVote, 'start_date')->widget(DateTimePicker::className(),[
                    'pluginOptions' => [
                            'format'=>'yyyy-mm-dd hh:ii:00',
                            'startDate' => date($datetime->format("Y-m-d")),
                            'todayHighlight' => true,
                            'autoclose'=>true,
                    ]
                ]) ?>
        </div>

        <div class="col-sm-6">
                <?= $form->field($modelVote, 'end_date')->widget(DateTimePicker::className(),[
                    'pluginOptions' => [
                        'format'=>'yyyy-mm-dd hh:ii:00',
                        'startDate' => date($datetime->format("Y-m-d")),
                        'todayHighlight' => true,
                        'autoclose'=>true,
                    ]
                ]) ?>
        </div>
    </div>
    <?php if (!$modelVote->isNewRecord) { ?>
            <?php echo $form->field($modelVote, 'vote_numbers')->textInput(['disabled'=>true])->label('Jumlah Pemilih')?>
    <?php } ?>
    <?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
        'widgetBody' => '.container-items', // required: css class selector
        'widgetItem' => '.item', // required: css class
        'limit' => 10, // the maximum times, an element can be cloned (default 999)
        'min' => 1, // 0 or 1 (default 1)
        'insertButton' => '.add-item', // css class
        'deleteButton' => '.remove-item', // css class
        'model' => $modelsCandidate[0],
        'formId' => 'dynamic-form',
        'formFields' => [
            'candidate_name',
            'candidate_desc',
        ],
    ]); ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-user"></i> Kandidat
            <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> Tambah Kandidat</button>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body container-items"><!-- widgetContainer -->
            <?php foreach ($modelsCandidate as $index => $modelCandidate): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <span class="panel-title-address">Kandidat ke : <?= ($index + 1) ?></span>
                        <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                        // necessary for update action.
                        if (!$modelCandidate->isNewRecord) {
                            echo Html::activeHiddenInput($modelCandidate, "[{$index}]candidate_id");
                        }
                        ?>
                        <?= $form->field($modelCandidate, "[{$index}]candidate_name")->textInput(['maxlength' => true]) ?>
                        <?= $form->field($modelCandidate, "[{$index}]candidate_desc")->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?php DynamicFormWidget::end(); ?>
    <?php if ($modelCandidate->isNewRecord) { ?>
        <?= $form->field($modelImport,'fileImport')->fileInput()->label('File Pemilih') ?>
        <?= Html::a('Download Template', ['download'], ['class' => 'btn btn-success']) ?>
        <p class="help-block">Silahkan masukkan file excel daftar pemilih.</p>
    <?php } ?>
    <div class="form-group">
        <?= Html::submitButton($modelCandidate->isNewRecord ? 'Tambah' : 'Perbarui', ['class' => 'btn btn-primary','id'=>'add-vote']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
