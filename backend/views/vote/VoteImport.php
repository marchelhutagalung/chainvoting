<?php

/**
 * Created by PhpStorm.
 * User: MarchelHutagalung
 * Date: 3/25/2019
 * Time: 4:32 PM
 */

/* @var $this \yii\web\View */
/* @var $modelImport \yii\base\DynamicModel */
/* @var $model \backend\models\Voter */

use backend\models\Vote;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = 'Tambah Pemilih';
$this->params['breadcrumbs'][] = ['label' => 'Votes', 'url' => ['create']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="voter-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]);?>

    <?php
    $vote = Vote::find()->all();
    $listVote = ArrayHelper::map($vote,'vote_id','vote_title');
    ?>
    <?= $form->field($modelVoter, 'vote_id')->dropDownList($listVote)->label("Voting");?>
    <?= $form->field($modelImport,'fileImport')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('<span class="fa fa-upload"></span>&nbsp;Import',['class'=>'btn btn-success']);?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
