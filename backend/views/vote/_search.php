<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\VoteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vote-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'vote_title', [
        'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control transparent']
    ])->textInput()->input('text', ['placeholder' => "Nama Voting"])->label(false);?>
    <div class="form-group">
        <?= Html::submitButton('Cari', ['class' => 'btn btn-primary btn-md']) ?>
        <?= Html::a('Hapus', ['index'], ['class' => 'btn btn-default btn-md']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
