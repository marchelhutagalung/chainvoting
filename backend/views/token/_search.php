<?php

use backend\models\Vote;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\TokenSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="token-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php
    $vote = Vote::find()->all();
    $listVote = ArrayHelper::map($vote,'vote_id','vote_title');
    ?>
    <?= $form->field($model, 'vote_id')->dropDownList($listVote,['prompt'=>'Pilih Vote..'])->label("Pilih Voting");?>
    <div class="form-group">
        <?= Html::submitButton('Cari', ['class' => 'btn btn-primary btn-md']) ?>
        <?= Html::a('Hapus', ['index'], ['class' => 'btn btn-default btn-md']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
