<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\TokenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bitcoin Address';
$this->params['breadcrumbs'][] = $this->title;
$uiHelper=\Yii::$app->uiHelper;

$cek = Yii::$app->getRequest()->getQueryParam('TokenSearch')['vote_id'];

?>
<div class="token-index">
    <?= $uiHelper->renderLine(); ?>
    <?=$uiHelper->beginContentRow() ?>
    <?=$uiHelper->beginContentBlock(['id' => 'grid-system2',
        'width' => 12,
    ]); ?>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= $uiHelper->endContentBlock()?>
    <?=$uiHelper->endContentRow() ?>

    <?=$uiHelper->beginContentRow() ?>
    <?=$uiHelper->beginContentBlock(['id' => 'grid-system2',
        'header' => 'Belum Digunakan',
        'width' => 12,
        'type' => 'success'
    ]); ?>
    <?php if($cek != null) {
            if($dataProvider->getTotalCount()> 0){
                    echo '<button class="btn btn-success pull-right" id="payall">Aktivasi Semua</button>';
                }
        ?>
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class'=>'yii\grid\SerialColumn'],
                ['attribute' => 'bitcoin_address','label' => 'Bitcoin Address'],
                ['attribute' => 'is_paid','format' => 'raw','label'=>'Status',
                    'value' => function($model){
                        if($model->is_paid!=1){
                            return '<span class="label label-danger"><i>Belum Aktif</i></span>';
                        }
                        else{
                            return '<span class="label label-success"><i>Aktiv</i></span>';
                        }
                    }
                ],
                [
                    'attribute' => 'vote_id',
                    'value'=>'vote.vote_title',
                    'label'=>'Voting'
                ],
                [
                    'class'=>'yii\grid\ActionColumn',
                    'header'=>'Operasi',
                    'headerOptions' => ['style'=>'color:#3c8dbc'],
                    'template' => '{pay}',
                    'buttons' => [
                        'pay'=> function ($url,$model){
                            if($model->is_paid==0){
                                return Html::button('Aktivasi',['value'=>$model->id,'class'=>'validation btn btn-success']);
                            }
                            else{
                                return '';
                            }
                        }
                    ],
                ]
            ]
        ]);
    ?>
    <?= $uiHelper->endContentBlock()?>
    <?=$uiHelper->endContentRow() ?>

    <?=$uiHelper->beginContentRow() ?>
    <?=$uiHelper->beginContentBlock(['id' => 'grid-system2',
        'header' => 'Sudah Digunakan',
        'width' => 6,
        'type' => 'danger'
    ]); ?>

    <?= GridView::widget(
        [
            'dataProvider' => $dataProviderUsed,
            'columns' => [
                ['class'=>'yii\grid\SerialColumn'],
                ['attribute' => 'bitcoin_address','label' => 'Bitcoin Address'],
                [
                    'attribute' => 'vote_id',
                    'label'=>'Voting',
                    'format' => 'raw',
                    'value'=>function($model){
                        return substr($model->vote->vote_title,0,16).' ...';
                    }
                ],
            ]
        ]
    )
    ?>
    <?= $uiHelper->endContentBlock()?>
    <?=$uiHelper->beginContentBlock(['id' => 'grid-system2',
        'header' => 'Kadaluarsa',
        'width' => 6,
        'type' => 'danger'
    ]); ?>

    <?= GridView::widget(
        [
            'dataProvider' => $dataProviderExp,
            'columns' => [
                ['class'=>'yii\grid\SerialColumn'],
                ['attribute' => 'bitcoin_address','label' => 'Bitcoin Address'],
                [
                    'attribute' => 'vote_id',
                    'label'=>'Voting',
                    'format' => 'raw',
                    'value'=>function($model){
                        return substr($model->vote->vote_title,0,16).'...';
                    }
                ],
            ]
        ]
    )
    ?>
    <?= $uiHelper->endContentBlock()?>
    <?=$uiHelper->endContentRow() ?>
    <?php } ?>
</div>
