<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Candidate */

$this->title = $model->candidate_name;
$this->params['breadcrumbs'][] = ['label' => 'Candidates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="candidate-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'candidate_name',
            [
                'attribute'=>'candidate_desc',
                'format'=>'raw',
                'label'=>'Description',
            ],
            [
                'attribute'=>'vote_id',
                'value'=>$model->vote->vote_title,
                'label'=>'Voting',
            ],
        ],
    ]) ?>

</div>
