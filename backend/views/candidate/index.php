<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CandidateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kandidat';
$this->params['breadcrumbs'][] = $this->title;
$uiHelper=\Yii::$app->uiHelper;
?>
<div class="candidate-index">
    <?=$uiHelper->beginContentRow() ?>

    <?=$uiHelper->beginContentBlock(['id' => 'grid-system2',
        'width' => 12,
        'type' => 'success'
    ]); ?>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //Html::a('Create Candidate', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'candidate_id',
            'candidate_name',
            [
                'attribute' => 'candidate_desc',
                'format' => 'raw',
            ],
            [
                'attribute'=>'vote_id',
                'value'=>'vote.vote_title',
                'label' => 'Voting'
            ],
//            'created_at',
            //'updated_at',
        ],
    ]); ?>
    <?= $uiHelper->endContentBlock()?>

    <?=$uiHelper->endContentRow() ?>

</div>
