<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Vote;
/* @var $this yii\web\View */
/* @var $model backend\models\Candidate */
/* @var $form yii\widgets\ActiveForm */
$uiHelper=\Yii::$app->uiHelper;
?>

<div class="candidate-form">
    <?=$uiHelper->beginContentRow() ?>

    <?=$uiHelper->beginContentBlock(['id' => 'grid-system2',
        'width' => 12,
        'type' => 'success'
    ]); ?>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'candidate_name')->textInput(['maxlength' => true])?>

    <?= $form->field($model, 'candidate_desc')->widget(\yii\redactor\widgets\Redactor::className()) ?>

    <?php
        $vote = Vote::find()->all();
        $listVote = ArrayHelper::map($vote,'vote_id','vote_title');
    ?>

    <?= $form->field($model, 'vote_id')->dropDownList($listVote);?>
    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?= $uiHelper->endContentBlock()?>

<?=$uiHelper->endContentRow() ?>
