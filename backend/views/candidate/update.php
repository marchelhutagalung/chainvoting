<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Candidate */

$this->title = 'Update Candidate: ' . $model->candidate_id;
$this->params['breadcrumbs'][] = ['label' => 'Candidates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->candidate_id, 'url' => ['view', 'id' => $model->candidate_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="candidate-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
