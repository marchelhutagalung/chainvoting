<?php

use backend\models\Vote;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\CandidateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="candidate-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'candidate_name', [
        'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control transparent']
    ])->textInput()->input('text', ['placeholder' => "Nama Kandidat"])->label(false);?>

    <?php
    $vote = Vote::find()->all();
    $listVote = ArrayHelper::map($vote,'vote_id','vote_title');
    ?>
    <?= $form->field($model, 'vote_id')->dropDownList($listVote,['prompt'=>'Pilih Vote..'])->label("Vote to");?>
    <div class="form-group">
        <?= Html::submitButton('Cari', ['class' => 'btn btn-primary btn-md']) ?>
        <?= Html::a('Hapus', ['index'], ['class' => 'btn btn-default btn-md']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
