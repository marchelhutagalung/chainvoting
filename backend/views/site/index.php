<?php
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
$uiHelper=\Yii::$app->uiHelper;
?>
<div class="site-index">
    <?=$uiHelper->beginContentRow() ?>

    <?=$uiHelper->beginContentBlock(['id' => 'grid-system2',
        'width' => 12,
        'type' => 'success',
        'header'=>'Voting',
    ]); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?= number_format($data['data']['balance'],4)?><sup style="font-size: 20px">BTC</sup></h3>

                        <p>Balance</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-bitcoin"></i>
                    </div>
                    <a href="https://chain.so/address/BTCTEST/moU5rEGoGaTXSPWjBKWMC68aLCbz7TQr6a" class="small-box-footer" target="_blank">Detail <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><?= $result['voting']?><sup style="font-size: 20px"> Voting</sup></h3>

                        <p>Total Voting</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <a href="../web/vote/index" class="small-box-footer" target="_blank">Detail <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?=$result['voters']?> Orang</h3>

                        <p>Pemilih terdaftar</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a href="../web/token/index" class="small-box-footer" target="_blank">Detail <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
       <!--     <div class="col-lg-3 col-xs-6">
                <div>
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3><?= $result['used']?><sup style="font-size: 15px"> Orang Sudah Voting</sup></h3>
                            <p><strong><?=$result['notUsed']?> </strong> Orang belum Voting</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-map-pin"></i>
                        </div>
                        <a href="../web/token/index" class="small-box-footer" target="_blank">Detail <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
            !-->
        </div>
        <?=$uiHelper->beginContentRow() ?>

        <?=$uiHelper->beginContentBlock(['id' => 'grid-system2',
            'width' => 12,
            'type' => 'danger',
            'header' => 'Daftar voting yang sedang berlangsung',
        ]); ?>
            <div class="col-sm-12 col-md-12">
                <div class="box box-solid">

                    <?php
                    $time =date("Y-m-d H:i:s");
                    $today = date('Y-m-d H:i:s',strtotime('+7 hour',strtotime($time)));
                    $count =0;
                    foreach ($modelVote as $item) {
                    $end = $item->end_date;
                    if($today<=$end) {
                        $count++;
                    ?>
                    <div class="box-body">
                        <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                        </h4>
                        <div class="media">
                            <div class="media-left">
                                    <img src="<?= Yii::$app->request->baseUrl . '/images/evoting.jpg'?>" style="width: 250px;height: 130px;border-radius: 4px;box-shadow: 0 1px 3px rgba(0,0,0,.15);">
                            </div>
                            <div class="media-body">
                                <div class="clearfix">
                                    <p class="pull-right">
                                        <a href="../web/vote/view?id=<?=$item->vote_id?>" class="btn btn-md btn-info" target="_blank">Rincian</a>
                                    </p>

                                    <h4 style="margin-top: 0"><?=$item->vote_title?></h4>

                                    <p><?=$item->vote_desc?></p>
                                    <p style="margin-bottom: 0">
                                        <i class="fa fa-users"></i> <?=$item->vote_numbers?> Orang
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }
                    } if($count == 0){
                        echo '<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;"> Tidak ada
                        </h4>';
                    } ?>
                </div>
            </div>
        <?= $uiHelper->endContentBlock()?>
        <?=$uiHelper->endContentRow() ?>
    </div>
<?= $uiHelper->endContentBlock()?>
<?=$uiHelper->endContentRow() ?>