<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Beranda', 'icon' => 'dashboard', 'url' => ['/']],
                    [
                        'label'=>'Daftar Voting',
                        'icon'=>'list',
                        'url' => ['vote/index'],
                    ],
                    [
                        'label'=>'Tambah Voting',
                        'icon'=>'envelope',
                        'url' => ['vote/vote-add'],
                    ],
                    [
                        'label'=>'Daftar Bitcoin',
                        'icon'=>'bitcoin',
                        'url'=>['token/index'],
                    ],
//                    [
//                        'label' => 'Kandidat',
//                        'icon' => 'user',
//                        'url' => '#',
//                        'items'=>[
                            [
                                'label'=>'Daftar Kandidat',
                                'icon'=>'users',
                                'url' => ['candidate/index'],
                            ],
//                            [
//                                'label'=>'Tambah Kandidat',
//                                'icon'=>'user-plus',
//                                'url' => ['candidate/create'],
//                            ],
//                        ],
//                    ],
//                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                ],
            ]
        ) ?>

    </section>

</aside>
