<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "candidate".
 *
 * @property int $candidate_id
 * @property string $candidate_name
 * @property string $candidate_desc
 * @property int $vote_id
 * @property int $deleted
 * @property string $deleted_at
 * @property string $deleted_by
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property Vote $vote
 */
class Candidate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'candidate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['candidate_name'], 'required'],
            [['deleted'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['candidate_name', 'candidate_desc'], 'string', 'max' => 255],
            [['vote_id'], 'string', 'max' => 3],
            [['deleted_by', 'created_by', 'updated_by'], 'string', 'max' => 32],
            [['vote_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vote::className(), 'targetAttribute' => ['vote_id' => 'vote_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'candidate_id' => 'Candidate ID',
            'candidate_name' => 'Candidate Name',
            'candidate_desc' => 'Candidate Desc',
            'vote_id' => 'Vote ID',
            'deleted' => 'Deleted',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVote()
    {
        return $this->hasOne(Vote::className(), ['vote_id' => 'vote_id']);
    }
}
