<?php

namespace backend\modules\api\models;

use Yii;

/**
 * This is the model class for table "token".
 *
 * @property int $id
 * @property string $token
 * @property string $bitcoin_pubkey
 * @property string $bitcoin_pvkey
 * @property string $bitcoin_address
 * @property int $is_used
 * @property int $is_paid
 * @property int $vote_id
 * @property int $deleted
 * @property string $deleted_at
 * @property string $deleted_by
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property Vote $vote
 */
class Token extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'token';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_used', 'is_paid', 'deleted'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['token'], 'string', 'max' => 11],
            [['bitcoin_pubkey', 'bitcoin_pvkey', 'bitcoin_address'], 'string', 'max' => 255],
            [['vote_id'], 'string', 'max' => 3],
            [['deleted_by', 'created_by', 'updated_by'], 'string', 'max' => 32],
            [['vote_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vote::className(), 'targetAttribute' => ['vote_id' => 'vote_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'token' => 'Token',
            'bitcoin_pubkey' => 'Bitcoin Pubkey',
            'bitcoin_pvkey' => 'Bitcoin Pvkey',
            'bitcoin_address' => 'Bitcoin Address',
            'is_used' => 'Is Used',
            'is_paid' => 'Is Paid',
            'vote_id' => 'Vote ID',
            'deleted' => 'Deleted',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVote()
    {
        return $this->hasOne(Vote::className(), ['vote_id' => 'vote_id']);
    }

}
