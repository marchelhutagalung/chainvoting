<?php
/**
 * Created by PhpStorm.
 * User: MarchelHutagalung
 * Date: 3/28/2019
 * Time: 6:30 PM
 */

namespace backend\modules\api\controllers;
use frontend\models\Candidate;
use yii\web\Controller;


class CandidateController extends Controller
{
    public $format = \yii\web\Response::FORMAT_JSON;

    /**
     * Digunakan untuk mengambil data kandidat berdasarkan vote id
     * @param $id
     * @return array
     */
    public function actionIndex($id){
            \Yii::$app->response->format = $this->format;
            $candidate = Candidate::find()
                ->where(['vote_id'=>$id])->all();

            if(count($candidate)>0){
                return array('status'=>true,'content'=>$candidate);
            }else{
               return array('status'=>false,'content'=>$candidate);
            }
    }

    /**
     * digunakan untuk mengambil data kandidat
     * @param $id
     * @return array
     */
    public function actionGet($id){
        \Yii::$app->response->format = $this->format;
        $candidate = Candidate::find()
            ->select('candidate_id,candidate_name')
            ->where(['vote_id'=>$id])->all();

        if(count($candidate)>0){
            return array('status'=>true,'content'=>$candidate);
        }else{
            return array('status'=>false,'content'=>$candidate);
        }
    }
}