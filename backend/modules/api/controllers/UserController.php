<?php

namespace backend\modules\api\controllers;
use backend\modules\api\models\User;


class UserController extends \yii\web\Controller
{
    /**
     * @return array
     * Mengambil data Admin
     */
    public function actionAdmin(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user = User::find()->where(['username'=>'administrator'])->one();

        if(count($user)>0){
            return array('status'=>true,'data'=>$user);
        }else{
            return array('status'=>false,'data'=>$user);
        }
    }
} 
