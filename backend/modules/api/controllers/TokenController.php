<?php

namespace backend\modules\api\controllers;

use Yii;
use yii\web\Controller;
use backend\modules\api\models\Token;

class TokenController extends Controller
{
    public $format = \yii\web\Response::FORMAT_JSON;

    /**
     * Mengambil address berdasarkan token yang digunakan voter
     * @param $id
     * @return array
     *
     */
    public function actionGetAddress($id){
        \Yii::$app->response->format = $this->format;
        $token = Token::find()->where(['id'=>$id])->one();

        if(count($token)>0){
            return array('status'=>true,'data'=>$token);
        }
        else{
            return array('status'=>false,'data'=>$token);
        }
    }

    public function actionPaid($id){
        \Yii::$app->response->format = $this->format;
        $token = Token::find()->where(['id'=>$id])->one();

        if(count($token)> 0){
            $token->is_paid = 1;
            if($token->save(false)){
                return array('status'=> true);
            }
            else{
                return array('status'=>false,'data'=>'Error');
            }
        }else{
            return array('status'=>false,'data'=>'Error');
        }
    }

    public function actionActive($id){
        \Yii::$app->response->format = $this->format;
        $token = Token::find()->where(['bitcoin_address'=>$id])->one();

        if(count($token)> 0){
            $token->is_paid = 1;
            if($token->save(false)){
                return array('status'=> true);
            }
            else{
                return array('status'=>false,'data'=>'Error');
            }
        }else{
            return array('status'=>false,'data'=>'Error');
        }
    }

    public function actionBitcoin($id){
        \Yii::$app->response->format = $this->format;
        $token = Token::find()->where(['id'=>$id])->one();
        if(is_object($token)){
            return array('status'=>true,'content'=>$token);
        }
        else{
            return array('status'=>false,'content'=>'false');
        }
    }

    public function actionPrivate($tkn){
        \Yii::$app->response->format = $this->format;
        $token = Token::find()
            ->select('id,bitcoin_pvkey')
            ->where(['token'=>$tkn])->one();
        if(is_object($token)){
            return array('status'=>true,'content'=>$token);
        }
        else{
            return array('status'=>false,'content'=>'false');
        }
    }

    public function actionUsed($tkn){
        \Yii::$app->response->format = $this->format;
        $token = Token::find()
            ->select('id,bitcoin_pvkey')
            ->where(['token'=>$tkn])->one();
        if(is_object($token)){
            $token->is_used = 1;
            if($token->save(false)){
                return array('status'=>true);
            }
        }
        else{
            return array('status'=>false);
        }
    }

    public function actionToken($id){
        \Yii::$app->response->format = $this->format;

        $model = Token::find()->select('bitcoin_address,bitcoin_pubkey')->where(['vote_id'=>$id])->andWhere(['is_paid'=>0])->all();

        if(count($model)>0){
            return array('status'=>true,'content'=>$model);
        }
        else{
            return array('status'=>false,'content'=>'false');
        }
    }
}