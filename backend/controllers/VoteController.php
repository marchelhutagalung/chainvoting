<?php

namespace backend\controllers;


use backend\models\Model;
use backend\models\Candidate;
use backend\models\Voter;
use BitcoinPHP\BitcoinECDSA\BitcoinECDSA;
use Yii;
use backend\models\Vote;
use backend\models\search\VoteSearch;

use yii\base\DynamicModel;
use yii\bootstrap\ActiveForm;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
/**
 * VoteController implements the CRUD actions for Vote model.
 */
class VoteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index','view','vote-update','vote-add','download'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Vote models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect('site/login');
        }
        else{
            $searchModel = new VoteSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $time =date("Y-m-d H:i:s");
        $today = date('Y-m-d H:i:s',strtotime('+7 hour',strtotime($time)));
        $start = Yii::$app->formatter->asDatetime($model->start_date,'php:Y-m-d H:i:s');
        $end = Yii::$app->formatter->asDatetime($model->end_date,'php:Y-m-d H:i:s');

        if($today<$start){
            \Yii::$app->getSession()->setFlash('warning', '<b>Perhatian!</b>&nbsp; Voting Belum dimulai.');
        }
        else if($today >= $start && $today <= $end){
            \Yii::$app->getSession()->setFlash('info', '<b>Info!</b>&nbsp; Voting sedang berlangsung.');
        }
        else{
            \Yii::$app->getSession()->setFlash('success', '<b>Info!</b>&nbsp; Voting Telah Berakhir.');
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
//    public function actionDelete($id)
//    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
//    }

    /**
     * Finds the Vote model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vote the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vote::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    /**
     * @return array|string|Response
     * @throws NotFoundHttpException
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function actionVoteAdd(){
        $modelVote = new Vote;
        $modelsCandidate = [new Candidate];
        $modelVoter = new Voter();
        $modelImport = new DynamicModel([
            'fileImport'=>'File Import',
        ]);
        $modelImport->addRule(['fileImport'], 'required');
        $modelImport->addRule(['fileImport'], 'file', ['extensions' => 'ods,xls,xlsx'], ['maxSize' => 1024 * 1024]);

        if ($modelVote->load(Yii::$app->request->post())) {

            $modelVote->vote_id = $this->generateId();
            $modelVote->bitcoin_address = $this->generateAddress();
            $modelsCandidate = Model::createMultiple(Candidate::classname());
            Model::loadMultiple($modelsCandidate, Yii::$app->request->post());
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsCandidate),
                    ActiveForm::validate($modelVote)
                );
            }
            if ($modelVote->save(false)) {
                foreach ($modelsCandidate as $modelCandidate) {
                    $modelCandidate->vote_id = $modelVote->vote_id;
                    $flag = $modelCandidate->save(false);
                }
            }

            if ($flag) {
                $modelImport->fileImport = \yii\web\UploadedFile::getInstance($modelImport, 'fileImport');
                $modelVoteNew = $this->findModel($modelVote->vote_id);
                if($modelImport->fileImport && $modelImport->validate()){
                    $inputFileType = \PHPExcel_IOFactory::identify($modelImport->fileImport->tempName);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($modelImport->fileImport->tempName);
                    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                    $baseRow = 2;
                    $errorEmail = array();
                    $temp_email = array();
                    while (!empty($sheetData[$baseRow]['A'])) {
                        /*Email Sementara */
                        $temp_email[$sheetData[$baseRow]['A']] = $sheetData[$baseRow]['B'];
                        $baseRow++;
                    }
                    /*Cek Duplicate*/
                    $result = array_unique($temp_email);
                    foreach ($result as $name => $email){
                        $modelVoter->subject = 'Voting '. $modelVote->vote_title;
                        $modelVoter->body = '<br> Hi ' . $name . ',<br> Berikut token anda untuk melakukan voting di <strong>' . $modelVote->vote_title . '</strong><br>
                      <h3>Token : <b>' . $modelVoter->generateToken(1, $modelVote->vote_id) . '</b></h3><br> Berikut Link untuk mengakses voting<br>  
                      <h2>
                        <a href="http://localhost/ChainVoting/frontend/web/voter/get-vote?id=' . $modelVote->vote_id . '">Klik disini</a>
                      </h2>';
                        if ($modelVoter->sendMail($email)) {
                            Yii::$app->session->setFlash('success', 'Pemilih berhasil didaftarkan.');
                        }
                        else{
                            $errorEmail[] = $email;
                        }
                    }
                    if(!empty($errorEmail)){
                        foreach ($errorEmail as $item){
                            Yii::$app->session->addFlash('error', 'Gagal Mengirim Email. Ke '.$item);
                        }
                    }

                    $modelVoteNew->vote_numbers = count($result);
                    if($modelVoteNew->save(false)){
                        return $this->redirect(['view', 'id' => $modelVote->vote_id]);
                    }
                }
            }
        }
        return $this->render('voteCreate', [
            'modelVote' => $modelVote,
            'modelsCandidate' => (empty($modelsCandidate)) ? [new Candidate] : $modelsCandidate,
            'modelImport'=>$modelImport,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionVoteUpdate($id){
        $modelVote = $this->findModel($id);
        $modelsCandidate = Candidate::find()->where(['vote_id'=>$id])->all();
        $time =date("Y-m-d H:i:s");
        $today = date('Y-m-d H:i:s',strtotime('+7 hour',strtotime($time)));

        if($today <= $modelVote->start_date) {
            if($modelVote->load(Yii::$app->request->post())){
                $oldIDs = ArrayHelper::map($modelsCandidate, 'candidate_id', 'candidate_id');
                $modelsCandidate = Model::createMultiple(Candidate::classname(), $modelsCandidate);
                Model::loadMultiple($modelsCandidate, Yii::$app->request->post());
                $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsCandidate, 'candidate_id', 'candidate_id')));
                if($modelVote->save(false)){
                    if(!empty($deletedIDs)){
                        Candidate::deleteAll(['candidate_id'=>$deletedIDs]);
                    }
                    foreach ($modelsCandidate as $modelCandidate){
                        $modelCandidate->vote_id = $modelVote->vote_id;
                        $flag = $modelCandidate->save(false);
                    }
                }
                if ($flag) {
                    return $this->redirect(['view', 'id' => $modelVote->vote_id]);
                }
            }
            return $this->render('VoteUpdate', [
                'modelVote' => $modelVote,
                'modelsCandidate' => (empty($modelsCandidate)) ? [new Candidate] : $modelsCandidate,
            ]);
        } else{
            Yii::$app->session->addFlash('error', 'Voting tidak dapat diperbarui karena voting sedang berlangsung');
            return $this->redirect(['view', 'id' => $id]);
        }
    }

    /**
     * Generate Token
     * @return string
     *
     */
    private function generateId(){
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";
        for($i = 0; $i<3;$i++){
            $res.= $chars[mt_rand(0,strlen($chars)-1)];
        }
        return $res;
    }

    /**
     * @return String
     * @throws \Exception
     */
    private function generateAddress(){
        $bitcoin = new BitcoinECDSA();
        $bitcoin->setNetworkPrefix('6f');
        $bitcoin->generateRandomPrivateKey();
        return $bitcoin->getAddress();
    }

    public function actionDownload(){
        $path = Yii::getAlias('@backend').'/web/template/';
        $file = $path . 'voters.xlsx';

        if (file_exists($file)) {
            Yii::$app->response->sendFile($file);
        }
    }
}
