<?php

namespace backend\controllers;

use backend\models\search\VoteSearch;
use Yii;
use backend\models\Token;
use backend\models\search\TokenSearch;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TokenController implements the CRUD actions for Token model.
 */
class TokenController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Token models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TokenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProviderUsed = new ActiveDataProvider([
            'query' => Token::find()->where(['is_used' => 1])->orderBy(['is_paid' => SORT_ASC]),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $dataProviderExp = $searchModel->searchex(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderUsed'=>$dataProviderUsed,
            'dataProviderExp'=>$dataProviderExp
        ]);
    }
}
