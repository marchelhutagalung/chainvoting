<?php
namespace backend\controllers;

use backend\models\Token;
use backend\models\Vote;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\backend\models\User;
use yii\helpers\Json;
use yii\httpclient\Client;
use common\models\LoginForm;
/**
 * Site controller
 */
class SiteController extends Controller
{
    public $BASE_URL = 'https://chain.so/api/v2/address/BTCTEST/';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect('site/login');
        }
        else{
        $result = array();
        $count = 0;
        $countUsed = 0;
        $countNotUsed = 0;
        $modelVote = Vote::find()->all();
        $voterUsed = Token::find()->all();

        foreach ($modelVote as $item){
            $count += $item->vote_numbers;
        }
        foreach ($voterUsed as $item){
            if($item->is_used==1){
                $countUsed++;
            }else{
                $countNotUsed++;
            }
        }
        $result['voters'] = $count;
        $result['voting'] = count($modelVote);
        $result['used'] = $countUsed;
        $result['notUsed']= $countNotUsed;
        $client = new Client();

        $response = $client->createRequest()
                    ->setMethod('GET')
                    ->setUrl($this->BASE_URL.'moU5rEGoGaTXSPWjBKWMC68aLCbz7TQr6a')
                    ->send();
            if($response->isOk){
                $data = Json::decode($response->content);

                return $this->render('index',
                    [
                        'data'=>$data,
                        'result'=>$result,
                        'modelVote'=>$modelVote,
                    ]);
            }
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionTest(){
        echo Yii::$app->homeUrl;
        echo Yii::$app->getHomeUrl();
    }
}
