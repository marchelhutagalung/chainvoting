<?php
return [
    'adminEmail' => 'chainvoting@gmail.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
